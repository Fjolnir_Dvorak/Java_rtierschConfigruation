/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.configruation.cli;

/**
 * Add new options to the Options parser.
 */
public interface IOptionsRegister {


    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName    Short representation of the option.
     * @param p_longName     Short descriptional representation of the
     *                       optional.
     * @param p_option       Does this option has arguments?
     * @param p_argumentName argument description.
     * @param p_description  option description.
     * @param p_required     is this option necessary to run the program?
     * @return true, if option was created.
     */
    boolean registerOption(final char p_shortName, final String p_longName,
                           final boolean p_option, final String p_argumentName,
                           final String p_description, final boolean p_required);

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName    Short representation of the option.
     * @param p_option       Does this option has arguments?
     * @param p_argumentName argument description.
     * @param p_description  option description.
     * @param p_required     is this option necessary to run the program?
     * @return true, if option was created.
     */
    boolean registerOption(final char p_shortName, final boolean p_option,
                           final String p_argumentName, final String p_description,
                           final boolean p_required);

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName   Short representation of the option.
     * @param p_option      Does this option has arguments?
     * @param p_description option description.
     * @param p_required    is this option necessary to run the program?
     * @return true, if option was created.
     */
    boolean registerOption(final char p_shortName, final boolean p_option,
                           final String p_description, final boolean p_required);

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName   Short representation of the option.
     * @param p_longName    Short descriptional representation of the optional.
     * @param p_option      Does this option has arguments?
     * @param p_description option description.
     * @param p_required    is this option necessary to run the program?
     * @return true, if option was created.
     */
    boolean registerOption(final char p_shortName, final String p_longName,
                           final boolean p_option, final String p_description,
                           final boolean p_required);

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_longName     Short descriptional representation of the
     *                       optional.
     * @param p_option       Does this option has arguments?
     * @param p_argumentName argument description.
     * @param p_description  option description.
     * @param p_required     is this option necessary to run the program?
     * @return true, if option was created.
     */
    boolean registerOption(final String p_longName, final boolean p_option,
                           final String p_argumentName, final String p_description,
                           final boolean p_required);

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_longName    Short descriptional representation of the optional.
     * @param p_option      Does this option has arguments?
     * @param p_description option description.
     * @param p_required    is this option necessary to run the program?
     * @return true, if option was created.
     */
    boolean registerOption(final String p_longName, final boolean p_option,
                           final String p_description, final boolean p_required);

}
