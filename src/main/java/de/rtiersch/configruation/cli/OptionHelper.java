/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.configruation.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.ArrayList;

/**
 * This is a utility class to interact with apache-cli-m_options. It isOfChar
 * all the important functions that need to interact with the Object Options
 * directly. It is recommendable to interact with this class through the
 * implemented interfaces to guarantee code readability and safety.
 */
public class OptionHelper
    implements IOptionsRegister
{
    /**
     * Registered short names of the m_options.
     */
    private final ArrayList<Character> m_shortNames = new ArrayList<>();
    /**
     * Registered long names of the m_options.
     */
    private final ArrayList<String>    m_longNames  = new ArrayList<>();
    /**
     * Options this class helps to interact with.
     */
    private final Options              m_options    = new Options();

    /**
     * Default constructor.
     */
    public OptionHelper()
    {
    }

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName    Short representation of the option.
     * @param p_longName     Short descriptional representation of the
     *                       optional.
     * @param p_option       Does this option has arguments?
     * @param p_argumentName argument description.
     * @param p_description  option description.
     * @param p_required     is this option necessary to run the program?
     * @return true, if option was created.
     */
    @Override
    public final boolean registerOption(
        final char p_shortName,
        final String p_longName,
        final boolean p_option,
        final String p_argumentName,
        final String p_description,
        final boolean p_required)
    {
        if (this.checkAddShortName(p_shortName) || this.checkAddLongName(
            p_longName))
        {
            return false;
        }
        this.m_options.addOption(Option.builder(Character.toString(p_shortName))
                                       .longOpt(p_longName)
                                       .hasArg(p_option)
                                       .argName(p_argumentName)
                                       .desc(p_description)
                                       .required(p_required)
                                       .build());
        return true;
    }

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName    Short representation of the option.
     * @param p_option       Does this option has arguments?
     * @param p_argumentName argument description.
     * @param p_description  option description.
     * @param p_required     is this option necessary to run the program?
     * @return true, if option was created.
     */
    @Override
    public final boolean registerOption(
        final char p_shortName,
        final boolean p_option,
        final String p_argumentName,
        final String p_description,
        final boolean p_required)
    {
        if (this.checkAddShortName(p_shortName)) {
            return false;
        }
        this.m_options.addOption(Option.builder(Character.toString(p_shortName))
                                       .hasArg(p_option)
                                       .argName(p_argumentName)
                                       .desc(p_description)
                                       .required(p_required)
                                       .build());
        return true;
    }

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName   Short representation of the option.
     * @param p_option      Does this option has arguments?
     * @param p_description option description.
     * @param p_required    is this option necessary to run the program?
     * @return true, if option was created.
     */
    @Override
    public final boolean registerOption(
        final char p_shortName,
        final boolean p_option,
        final String p_description,
        final boolean p_required)
    {
        return this.registerOption(p_shortName, p_option, null, p_description,
                                   p_required);
    }

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_shortName   Short representation of the option.
     * @param p_longName    Short descriptional representation of the optional.
     * @param p_option      Does this option has arguments?
     * @param p_description option description.
     * @param p_required    is this option necessary to run the program?
     * @return true, if option was created.
     */
    @Override
    public final boolean registerOption(
        final char p_shortName,
        final String p_longName,
        final boolean p_option,
        final String p_description,
        final boolean p_required)
    {
        return this.registerOption(p_shortName, p_longName, p_option, null,
                                   p_description, p_required);
    }

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_longName     Short descriptional representation of the
     *                       optional.
     * @param p_option       Does this option has arguments?
     * @param p_argumentName argument description.
     * @param p_description  option description.
     * @param p_required     is this option necessary to run the program?
     * @return true, if option was created.
     */
    @Override
    public final boolean registerOption(
        final String p_longName,
        final boolean p_option,
        final String p_argumentName,
        final String p_description,
        final boolean p_required)
    {
        if (this.checkAddLongName(p_longName)) {
            return false;
        }
        this.m_options.addOption(Option.builder()
                                       .longOpt(p_longName)
                                       .hasArg(p_option)
                                       .argName(p_argumentName)
                                       .desc(p_description)
                                       .required(p_required)
                                       .build());
        return true;
    }

    /**
     * Creates a new Option if that option is not already existent.
     *
     * @param p_longName    Short descriptional representation of the optional.
     * @param p_option      Does this option has arguments?
     * @param p_description option description.
     * @param p_required    is this option necessary to run the program?
     * @return true, if option was created.
     */
    @Override
    public final boolean registerOption(
        final String p_longName,
        final boolean p_option,
        final String p_description,
        final boolean p_required)
    {
        return this.registerOption(p_longName, p_option, null, p_description,
                                   p_required);
    }

    /**
     * Checks and adds the shortName to the internal list.
     * TODO why I am adding the shortname to the list?
     *
     * @param p_shortName short name of the option
     * @return false if it was not in the list before.
     */
    private boolean checkAddShortName(final char p_shortName)
    {
        if (this.m_shortNames.contains(new Character(p_shortName))) {
            return true;
        }
        this.m_shortNames.add(p_shortName);
        return false;
    }

    /**
     * Checks and adds the longName to the internal list.
     * TODO why I am adding the longname to the list?
     *
     * @param p_longName long name of the option
     * @return false if it was not in the list.
     */
    private boolean checkAddLongName(final String p_longName)
    {
        if (this.m_longNames.contains(p_longName)) {
            return true;
        }
        this.m_longNames.add(p_longName);
        return false;
    }
}
